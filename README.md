Intro Statistics Applets
================

-   [Two Proportion Resampling Test](#two-proportion-resampling-test)
-   [One Proportion Resampling Test](#one-proportion-resampling-test)
-   [Standard Deviation Game](#standard-deviation-game)
-   [Confidence Interval App](#confidence-interval-app)
-   [Gettysburg Submission App](#gettysburg-submission-app)
-   [Sampling Distribution for the Mean](#sampling-distribution-for-the-mean)

Intro Statistics Applets houses the Shiny apps developed for use in introductory statistics courses at Michigan State University. These applets are designed to be modified for your own use. A description of each applet is provided below, along with a link to a published version.

------------------------------------------------------------------------

### Two Proportion Resampling Test

The Two Proportion Resampling Test app conducts [Fisher's Exact Test](https://en.wikipedia.org/wiki/Fisher%27s_exact_test) by randomization. Preset datasets are included, with the option to customize the observed data. Download the *TwoProportionResamplingTest* folder and start customizing the app to your specified needs. A published version of this app can be seen [here](https://shiny.stt.msu.edu/fairbour/Demo/TwoPropResampling/).

<img src="img/TwoPropResamplingTest.png" width="2178" />

------------------------------------------------------------------------

### One Proportion Resampling Test

The One Proportion Resampling Test app conducts a one proportion hypothesis test by randomization. Preset examples are included, with the option to specify your own values of *n* and *p*. Download the *OneProportionResamplingTest* folder and start customizing the app to your specific needs. A published version of this app can be seen [here](https://shiny.stt.msu.edu/fairbour/Demo/OnePropResampling/).

<img src="img/OnePropResamplingTest.png" width="2178" />

------------------------------------------------------------------------

### Standard Deviation Game

The Standard Deviation Game let's you put your skills to the test! Four histograms are shown, and you need to select the histogram that matches the standard deviation provided. Two difficulties are available. Download the *StandardDeviationGame* folder and start customizing the app to your specific needs. A published version of this app can be seen [here](https://shiny.stt.msu.edu/fairbour/Demo/SDGame/).

<img src="img/StandardDeviationGame.png" width="2178" />

------------------------------------------------------------------------

### Confidence Interval App

This app allows users to visualize the repeated confidence intervals for a proportion. The true proportion, confidence level, sample size, and the number of samples can all be chosen by the user. Download the *ConfidenceIntervalApp* folder and start customizing the app to your specific needs. A published version of this app can be seen [here](https://shiny.stt.msu.edu/fairbour/Demo/Confidence/).

<img src="img/ConfidenceIntervalApp.png" width="2178" />

------------------------------------------------------------------------

### Gettysburg Submission App

This app is designed to collect and visualize class data from the Gettysburg Activity. The activity has students attempt to estimate the average word length in the Gettysburg address by sampling 10 words of their choice, and then by sampling 10 random words. Download the *GettysburgSubmissionApp* folder and start customizing the app to your specific needs. A published version of this app can be seen [here](https://shiny.stt.msu.edu/fairbour/Demo/GettySamples/).

<img src="img/GettysburgSubmissionApp.png" width="2178" />

------------------------------------------------------------------------

### Sampling Distribution for the Mean

This app is shows the sampling distribution for the mean for a dataset of the user's choice. Users also have the option to upload their own data via a .csv file. Download the *SamplingDistributionForMean* folder and start customizing the app to your specific needs. A published version of this app can be seen [here](https://shiny.stt.msu.edu/fairbour/Demo/SampDistMean/).

<img src="img/SamplingDistforMean.png" width="2178" />

------------------------------------------------------------------------
