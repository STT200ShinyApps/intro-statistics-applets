library(shiny)

ui <- fluidPage(
    
    titlePanel("COVID-19 Antibody Testing"),
    
    sidebarLayout(
        
        sidebarPanel(
            tags$div(class="header", checked=NA,
                     tags$p("This app uses information about COVID-19 antibody tests that were on the market in April 2020."),
                     tags$p("Tests are listed by the lab that creates them, followed by the reported sensitivity and specificity of the test."),
                     tags$p("Choose a test and base rate to see the calculated true positive and true negative rate for the test.")
            ),
            uiOutput("tab"),
            helpText(" "),
            hr(),
            tags$p("Written by Harold Wu"),
            selectInput("dataset", "Choose a test:",
                        choices = c("Abbott Laboratories (100%, 99.5%)",
                                    "Cellex (93.8%, 95.6%)",
                                    "Creative Diagnostics (94.5%, 100%)",
                                    "CTK Biotech (96.9%, 99.4%)",
                                    "Epitope Diagnostics - IgG (100%, 100%)",
                                    "Epitope Diagnostics - IgM (45%, 100%)",
                                    "Intec Products (95.2%, 98%)",
                                    "Nirmidas Biotech (93.8%, 99.5%)",
                                    "Ortho-Clinical Diagnostics (83.3%, 100%)",
                                    "SD Biosensor (81.8%, 96.6%)")),

            
            numericInput("base", "Base rate (%): Please use a number between 0-100", 5, min = 0, max = 100),
            
          
            actionButton("update", "Update View")
            
        ),
        
        mainPanel(
            
            
            h4("Contingency table "),
            tableOutput("view"),
            
            h4("Calculations"),
            verbatimTextOutput("clculates")
            
        )
        
    )
)

ABT <- c(1, 0.995)
CLX <- c(0.938, 0.956)
CTV <- c(0.945, 1)
CTK <- c(0.969, 0.994)
EPG <- c(1, 1)
EPM <- c(0.45, 1)
ITC <- c(0.952, 0.98)
NMD <- c(0.938, 0.995)
OCD <- c(0.833, 1)
SDB <- c(0.818, 0.966)

server <- function(input, output) {
    
    datasetInput <- eventReactive(input$update, {
        switch(input$dataset,
               "Abbott Laboratories (100%, 99.5%)" = ABT,
               "Cellex (93.8%, 95.6%)" = CLX,
               "Creative Diagnostics (94.5%, 100%)" = CTV,
               "CTK Biotech (96.9%, 99.4%)" = CTK,
               "Epitope Diagnostics - IgG (100%, 100%)" = EPG,
               "Epitope Diagnostics - IgM (45%, 100%)" = EPM,
               "Intec Products (95.2%, 98%)" = ITC,
               "Nirmidas Biotech (93.8%, 99.5%)" = NMD,
               "Ortho-Clinical Diagnostics (83.3%, 100%)" = OCD,
               "SD Biosensor (81.8%, 96.6%)" = SDB)
    }, ignoreNULL = FALSE)
    

    output$clculates <- renderPrint({
        row1 <- round(10000 * (input$base / 100), 4)
        a1 <- round(datasetInput()[1] * row1, 4)
        a2 <- round(row1 - a1, 4)
        row2 <- round(10000 - row1, 4)
        a4 <- round(datasetInput()[2] * row2, 4)
        a3 <- round(row2 - a4, 4)
        col1 <- round(a1 + a3, 4)
        col2 <- round(a2 + a4, 4)
        
        q1 <- round(a1/col1, 6)
        q2 <- round(a4/col2, 6)
        q3 <- round(a3/col1, 6)
        q4 <- round(a2/col2, 6)

        sol <- list("True positive rate" = q1,
                    "True negative rate" = q2,
                    "False positive rate" = q3,
                    "False negative rate" = q4)
        sol
    })
    
    output$view <- renderTable({
        
        row1 <- round(10000 * (input$base / 100), 4)
        a1 <- round(datasetInput()[1] * row1, 4)
        a2 <- round(row1 - a1, 4)
        row2 <- round(10000 - row1, 4)
        a4 <- round(datasetInput()[2] * row2, 4)
        a3 <- round(row2 - a4, 4)
        col1 <- round(a1 + a3, 4)
        col2 <- round(a2 + a4, 4)
        
        c_table <- data.frame("Antibody_Status" = c("Has antibodies", "No antibodies", "Total"),
                              "Test_Positive" = c(a1, a3, col1),
                              "Test_Negative" = c(a2, a4, col2),
                              "Total" = c(row1, row2, 10000))
        rownames(c_table) <- c("has antibody", "no antibody", "total")
        
        
        
        c_table
    })
    
}

shinyApp(ui, server)